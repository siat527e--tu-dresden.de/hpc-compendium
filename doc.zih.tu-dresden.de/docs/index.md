# ZIH HPC Documentation

This is the documentation of the HPC systems and services provided at
[TU Dresden/ZIH](https://tu-dresden.de/zih/). This documentation is work in progress, since we try
to incorporate more information with increasing experience and with every question you ask us. The
HPC team invites you to take part in the improvement of these pages by correcting or adding useful
information.

## Contribution

Your contributions are highly welcome. The easiest way for you to contribute is to report issues via
the GitLab
[issue tracking system](https://gitlab.hrz.tu-chemnitz.de/zih/hpcsupport/hpc-compendium/-/issues).
Please check for any already existing issue before submitting your issue in order to avoid duplicate
issues.

Please also find out the other ways you could contribute in our [guidelines how to contribute](contrib/howto_contribute.md).

!!! tip "Reminder"

    Non-documentation issues and requests need to be send to
    [hpcsupport@zih.tu-dresden.de](mailto:hpcsupport@zih.tu-dresden.de).

## News

**2022-01-13** [Supercomputing extension for TU Dresden](https://tu-dresden.de/zih/die-einrichtung/news/supercomputing-cluster-2022)

## Training and Courses

We offer a rich and colorful bouquet of courses from classical *HPC introduction* to various
*Performance Analysis* and *Machine Learning* trainings. Please refer to the page
[Training Offers](https://tu-dresden.de/zih/hochleistungsrechnen/nhr-training)
for a detailed overview of the courses and the respective dates at ZIH.

* [HPC introduction slides](misc/HPC-Introduction.pdf) (Sep. 2021)
